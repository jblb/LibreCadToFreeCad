#!/usr/bin/python

############################################################################
# LibreCadToFreeCad
#
# Author: Marc BERLIOUX <github@berlioux.com>
# Copyright: 2018 Marc BERLIOUX
# Licence: GPL3
# Home page: https://framagit.org/MarcusFecit/LibreCadToFreeCad
#
# LibreCadToFreeCad is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# LibreCadToFreeCad is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with LibreCadToFreeCad. If not, see <http://www.gnu.org/licenses/>.
#
############################################################################

import os, sys, getopt
import ezdxf
import math

maxDecimalsNumber=8 # All points coords are by default rounded to 8 decimals
fuckingFloatToBinaryError=1e-8 # Additional tolerance to avoid float to binary issues
digitsNumber=3 # default number of decimals for tolerance of coincidence tests
angleMinutes=float(1) # default tolerance in minutes of degree for tangentiality tests
documentName="DocumentName" # default FreeCAD document name, just in case
numericLayerPrefix="L_" # additionnal prefix for layers starting with a number
debugMode=False # Debug mode is off by default
exposeGeometry=False # Either to expose Ellipses internal geometry or not.
constraintLinesLengths=False # 
constraintRadius=False # 
constraintOrigins=False # 
constraintCoincidences=True # 
constraintTangents=True # 
constraintHorizontalLines=True # 
constraintVerticalLines=True # 
inputFile="" # Input file name
outputFile="" # Output file name
selectedLayer="" # The DXF's layer to process
visibleLayers=False #
entityCount=0 # DXF's processed entity counter
constraintCount=0
layerNames=[] # DXF's list of layer names
layerLineEntities=[] # [EntityNumber,x1,y1,x2,y2]
layerCircleEntities=[] # [EntityNumber,xCenter,yCenter,radius]
layerEllipseEntities=[] # [EntityNumber,xStart,yStart,xEnd,yEnd,startSlope,endSlope]
layerCircleArcEntities=[] # [EntityNumber,xCenter,yCenter,radius,startAngle,endAngle,xStart,yStart,xEnd,yEnd]
layerEllipseArcEntities=[] # [EntityNumber,xCenter,yCenter,radius,startAngle,endAngle,xStart,yStart,xEnd,yEnd]
fileHandle="" # Output file object
endOfLine="\n" # Default end of line

def usage():
  print ("Usage :")
  print (sys.argv[0] + " -i <input_file> [-o <output_file>] [-l <layer_to_process>] [-f <fixed_number_of_digits>] [-m <minutes_of_angle>] [-c <l,r,o,c,t,h,v,a>] [-g] [-v] [-d] [-h]")
  print ("")
  print ("Options:")
  print ("       -i <input_file>             : The DXF file to process. Mandatory")
  print ("       -o <output_file>            : Output file. If not specified, writes to stdout")
  print ("       -l <layer_to_process>       : Single layer to process. If not specified, all layers are processed")
  print ("       -f <fixed_number_of_digits> : Number of decimals to use for Coincidence Tolerance")
  print ("       -m <minutes_of_angle>       : Number of minutes of degrees to use for Tangentiality Tolerance")
  print ("       -c <l,r,o,c,t,h,v,a>        : Constraints selection. See description below")
  print ("       -g                          : Adds ellipses internal geometry")
  print ("       -v                          : Process only visible layers")
  print ("       -d                          : Shows debugging output")
  print ("       -h                          : This help")
  print ("")
  print ("Constraints selectors used in '-c' option:")
  print ("        l                          : Lines lengths")
  print ("        r                          : Radius lengths")
  print ("        o                          : Circles and Arcs centers fixed distance from origin")
  print ("        c                          : Entities Ends Coincidence")
  print ("        t                          : Entities Ends Tangentiality. Implies Coincidence..")
  print ("        h                          : Lines Horizontality")
  print ("        v                          : Lines Verticality")
  print ("        a                          : All supported constraints. Can result in over-constraint sketches..")
  print ("        0                          : Disable all constraints")
  print ("       These are active by default : r,c,t,h,v")

def processOptions(argv):
  global inputFile
  global outputFile
  global selectedLayer
  global visibleLayers
  global documentName
  global debugMode
  global exposeGeometry
  global digitsNumber
  global angleMinutes
  global fileHandle
  global endOfLine
  global constraintLinesLengths
  global constraintRadius
  global constraintOrigins
  global constraintCoincidences
  global constraintTangents
  global constraintHorizontalLines
  global constraintVerticalLines
  try:
    opts, args = getopt.getopt(argv,"dbghvi:o:l:f:m:c:",["debug","body","geometry","help","visible","ifile=","ofile=","layer=","fix=","minutes=","constraints="])
  except getopt.GetoptError:
    print ("Error: Wrong Options !\n")
    usage()
    sys.exit(2)
  for opt, arg in opts:
    if opt in ("-h", "--help"):
      usage()
      sys.exit(0)
    elif opt in ("-d", "--debug"):
      debugMode = True
    elif opt in ("-g", "--geometry"):
      exposeGeometry = True
    elif opt in ("-i", "--ifile"):
      inputFile = arg
    elif opt in ("-o", "--ofile"):
      outputFile = arg
    elif opt in ("-l", "--layer"):
      selectedLayer = arg
    elif opt in ("-v", "--visible"):
      visibleLayers = True
    elif opt in ("-f", "--fix"):
      digitsNumber = int(math.floor(float(arg)))
    elif opt in ("-m", "--minutes"):
      angleMinutes = int(math.floor(float(arg)))
    elif opt in ("-c", "--constraints"):
      cList = arg
      constraintOff=False
      constraintLinesLengths=False
      constraintRadius=False
      constraintOrigins=False
      constraintCoincidences=False
      constraintTangents=False
      constraintHorizontalLines=False
      constraintVerticalLines=False
      for cn in cList:
        if cn=="a":
          constraintLinesLengths=True
          constraintRadius=True
          constraintOrigins=True
          constraintCoincidences=True
          constraintTangents=True
          constraintHorizontalLines=True
          constraintVerticalLines=True
        if cn=="l":
          constraintLinesLengths=True
        if cn=="r":
          constraintRadius=True
        if cn=="o":
          constraintOrigins=True
        if cn=="c":
          constraintCoincidences=True
        if cn=="t":
          constraintCoincidences=True
          constraintTangents=True
        if cn=="h":
          constraintHorizontalLines=True
        if cn=="v":
          constraintVerticalLines=True
        if cn=="0":
          constraintOff=True
      if constraintOff:
        constraintLinesLengths=False
        constraintRadius=False
        constraintOrigins=False
        constraintCoincidences=False
        constraintTangents=False
        constraintHorizontalLines=False
        constraintVerticalLines=False
  if inputFile:
    try:
      f=open(inputFile,"r")
      f.close()
    except:
      print ("Error: Cannot find the specified file !")
      sys.exit(3)
    documentName=os.path.splitext(os.path.basename(inputFile))[0]
  else:
    print ("Error: Please, at least specify an input file !\n")
    usage()
    sys.exit(1)
  if outputFile:
    try:
      fileHandle=open(outputFile,"w")
    except:
      print ('Error: Cannot write to the specified file !')
      sys.exit(4)
  if sys.platform.startswith('win') or sys.platform.startswith('cygwin'):
    endOfLine="\r\n"
  if debugMode:
    print ("# Platform: "+str(sys.platform))
    print ("# Input File: "+str(os.path.abspath(inputFile)))
    if outputFile: print ("# Output File: "+str(outputFile))
    else: print ("# Output File: Terminal (stdout)")
    if selectedLayer: print ("# Selected Layer: "+str(selectedLayer))
    else:
      print ("# Selected Layer: All layers")
      if visibleLayers: print ("#  But only Visible Layers will be processed.. ")
    print ("# FreeCAD Document Name: "+str(documentName)+".fcstd")
    print ("# Active Constraints: ")
    if constraintLinesLengths: print ("## -> Lines Lengths")
    if constraintRadius: print ("## -> Radius")
    if constraintOrigins: print ("## -> Arcs and Cercles Centers Distance from Origin")
    if constraintCoincidences: print ("## -> Coincidences")
    if constraintTangents: print ("## -> Tangents")
    if constraintHorizontalLines: print ("## -> Horizontal Lines")
    if constraintVerticalLines: print ("## -> Vertical Lines")
    
def spitLine(l):
  global outputFile
  global fileHandle
  global endOfLine
  if outputFile:
    fileHandle.write(l)
    fileHandle.write(endOfLine)
  else:
    print(l)

def createDocument(dn):
  spitLine('from FreeCAD import Sketcher')
  spitLine('App.newDocument("'+str(dn)+'")')
  spitLine('App.setActiveDocument("'+str(dn)+'")')
  spitLine('App.ActiveDocument=App.getDocument("'+str(dn)+'")')
  if debugMode: print("# Document created: "+str(dn))

def saveFcstdFile(fn):
  spitLine('App.getDocument("'+fn.replace("-","_")+'").saveAs(u"'+fn+'.fcstd")')

def createSketch(sn):
  spitLine("App.activeDocument().addObject('Sketcher::SketchObject','"+str(sn)+"')")
  if sn[:3]=='XY_':
    spitLine("App.activeDocument()."+str(sn)+".Placement = App.Placement(App.Vector(0.000000,0.000000,0.000000),App.Rotation(0.000000,0.000000,0.000000,1.000000))") #XY
  elif sn[:3]=='XZ_':
    spitLine("App.activeDocument()."+str(sn)+".Placement = App.Placement(App.Vector(0.000000,0.000000,0.000000),App.Rotation(-0.707107,0.000000,0.000000,-0.707107))") #XZ
  elif sn[:3]=='YZ_':
    spitLine("App.activeDocument()."+str(sn)+".Placement = App.Placement(App.Vector(0.000000,0.000000,0.000000),App.Rotation(0.500000,0.500000,0.500000,0.500000))") #YZ
  else:
    spitLine("App.activeDocument()."+str(sn)+".Placement = App.Placement(App.Vector(0.000000,0.000000,0.000000),App.Rotation(0.000000,0.000000,0.000000,1.000000))") #XY
  spitLine("ActiveSketch = App.ActiveDocument.getObject('"+str(sn)+"')")

def processLayer(ln,nln):
  global entityCount
  global constraintCount
  entityCount=0
  constraintCount=0
  createSketch(nln)
  for e in msp:
    if e.dxf.layer==ln:
      if e.dxftype() == 'LINE': createLine(nln,e.dxf.start[0],e.dxf.start[1],e.dxf.end[0],e.dxf.end[1])
      if e.dxftype() == 'CIRCLE': createCircle(nln,e.dxf.center[0],e.dxf.center[1],e.dxf.radius)
      if e.dxftype() == 'ARC': createCircleArc(nln,e.dxf.center[0],e.dxf.center[1],e.dxf.radius,math.radians(e.dxf.start_angle),math.radians(e.dxf.end_angle))
      if e.dxftype() == 'ELLIPSE':
        if e.dxf.start_param==0 and e.dxf.end_param==6.28318530718: createEllipse(nln,e.dxf.center[0],e.dxf.center[1],e.dxf.major_axis[0],e.dxf.major_axis[1],e.dxf.ratio)
        else: createEllipseArc(nln,e.dxf.center[0],e.dxf.center[1],e.dxf.major_axis[0],e.dxf.major_axis[1],e.dxf.ratio,e.dxf.start_param,e.dxf.end_param)
      if e.dxftype()=="LWPOLYLINE":
        sp=e[0]
        bulgeRatio=sp[4]
        for i in range (1,len(e)):
          ep=e[i]
          if bulgeRatio==0:
            createLine(nln,sp[0],sp[1],ep[0],ep[1])
            entityCount=entityCount+1
          else:
            la=math.atan2(ep[1]-sp[1],ep[0]-sp[0]) # line angle
            d=(math.hypot(ep[0]-sp[0],ep[1]-sp[1]))/2 # chord half length
            xd=(ep[0]+sp[0])/2 # x chord mid point
            yd=(ep[1]+sp[1])/2 # y chord mid point
            r=abs(d*(bulgeRatio*bulgeRatio+1)/(bulgeRatio*2)) # arc radius
            angle=2*math.atan(bulgeRatio) # half arc angle
            if bulgeRatio > 0:
              xc=xd-math.sin(la)*(r-abs(bulgeRatio*d)) # x arc center
              yc=yd+math.cos(la)*(r-abs(bulgeRatio*d)) # y arc center
              a1=la-math.pi/2-angle # start point arc angle
              a2=la-math.pi/2+angle # end point arc angle
            else:
              xc=xd+math.sin(la)*(r-abs(bulgeRatio*d)) # x arc center
              yc=yd-math.cos(la)*(r-abs(bulgeRatio*d)) # y arc center
              a1=la+math.pi/2+angle # start point arc angle
              a2=la+math.pi/2-angle # end point arc angle
            createCircleArc(nln,xc,yc,r,a1,a2)
            entityCount=entityCount+1
          bulgeRatio=ep[4]
          sp=e[i]
        entityCount=entityCount-1
      entityCount=entityCount+1
  if debugMode: print("## Entities created for layer '"+str(nln)+"' : "+str(entityCount))

def testCoincidence(x1,y1,x2,y2):
  if abs(x1-x2)>=0 and abs(x1-x2)<=lineTolerance and abs(y1-y2)>=0 and abs(y1-y2)<=lineTolerance: return True
  else: return False

def testPerpendicularAngles(a1,a2):
  global angleTolerance
  testValue=90-abs(a1-a2)
  if testValue >= 0 and testValue <= angleTolerance: return True
  testValue=abs(a1-a2)-90
  if testValue >= 0 and testValue <= angleTolerance: return True
  testValue=270-abs(a1-a2)
  if testValue >= 0 and testValue <= angleTolerance: return True
  testValue=abs(a1-a2)-270
  if testValue >= 0 and testValue <= angleTolerance: return True
  else:
    return False

def testFlatAngles(a1,a2):
  global angleTolerance
  testValue=abs(a1-a2)
  if testValue >= 0 and testValue <= angleTolerance: return True
  testValue=abs(a1-a2)-180
  if testValue >= 0 and testValue <= angleTolerance: return True
  testValue=180-abs(a1-a2)
  if testValue >= 0 and testValue <= angleTolerance: return True
  else:
    return False

def stitchLinesToLines(ln):
  global layerLineEntities
  global constraintCount
  for i in range(0,len(layerLineEntities)-1):
    line1x1=layerLineEntities[i][1]
    line1y1=layerLineEntities[i][2]
    line1x2=layerLineEntities[i][3]
    line1y2=layerLineEntities[i][4]
    for j in range(i+1,len(layerLineEntities)):
      line2x1=layerLineEntities[j][1]
      line2y1=layerLineEntities[j][2]
      line2x2=layerLineEntities[j][3]
      line2y2=layerLineEntities[j][4]
      if testCoincidence(line1x1,line1y1,line2x1,line2y1):
        spitLine("App.ActiveDocument."+str(ln)+".addConstraint(Sketcher.Constraint('Coincident',"+str(layerLineEntities[i][0])+",1,"+str(layerLineEntities[j][0])+",1))")
        constraintCount=constraintCount+1
      if testCoincidence(line1x1,line1y1,line2x2,line2y2):
        spitLine("App.ActiveDocument."+str(ln)+".addConstraint(Sketcher.Constraint('Coincident',"+str(layerLineEntities[i][0])+",1,"+str(layerLineEntities[j][0])+",2))")
        constraintCount=constraintCount+1
      if testCoincidence(line1x2,line1y2,line2x1,line2y1):
        spitLine("App.ActiveDocument."+str(ln)+".addConstraint(Sketcher.Constraint('Coincident',"+str(layerLineEntities[i][0])+",2,"+str(layerLineEntities[j][0])+",1))")
        constraintCount=constraintCount+1
      if testCoincidence(line1x2,line1y2,line2x2,line2y2):
        spitLine("App.ActiveDocument."+str(ln)+".addConstraint(Sketcher.Constraint('Coincident',"+str(layerLineEntities[i][0])+",2,"+str(layerLineEntities[j][0])+",2))")
        constraintCount=constraintCount+1

def stitchCircleArcsToCircleArcs(ln):
  global layerCircleArcEntities
  global constraintCount
  for i in range(0,len(layerCircleArcEntities)-1):
    arc1a1=layerCircleArcEntities[i][3]
    arc1a2=layerCircleArcEntities[i][4]
    arc1x1=layerCircleArcEntities[i][5]
    arc1y1=layerCircleArcEntities[i][6]
    arc1x2=layerCircleArcEntities[i][7]
    arc1y2=layerCircleArcEntities[i][8]
    for j in range(i+1,len(layerCircleArcEntities)):
      arc2a1=layerCircleArcEntities[j][3]
      arc2a2=layerCircleArcEntities[j][4]
      arc2x1=layerCircleArcEntities[j][5]
      arc2y1=layerCircleArcEntities[j][6]
      arc2x2=layerCircleArcEntities[j][7]
      arc2y2=layerCircleArcEntities[j][8]
      if testCoincidence(arc1x1,arc1y1,arc2x1,arc2y1):
        if testFlatAngles(arc1a1,arc2a1) and constraintTangents:
          spitLine("App.ActiveDocument."+str(ln)+".addConstraint(Sketcher.Constraint('Tangent',"+str(layerCircleArcEntities[i][0])+",1,"+str(layerCircleArcEntities[j][0])+",1))")
          constraintCount=constraintCount+1
        else:
          spitLine("App.ActiveDocument."+str(ln)+".addConstraint(Sketcher.Constraint('Coincident',"+str(layerCircleArcEntities[i][0])+",1,"+str(layerCircleArcEntities[j][0])+",1))")
          constraintCount=constraintCount+1
      if testCoincidence(arc1x1,arc1y1,arc2x2,arc2y2):
        if testFlatAngles(arc1a1,arc2a2) and constraintTangents:
          spitLine("App.ActiveDocument."+str(ln)+".addConstraint(Sketcher.Constraint('Tangent',"+str(layerCircleArcEntities[i][0])+",1,"+str(layerCircleArcEntities[j][0])+",2))")
          constraintCount=constraintCount+1
        else:
          spitLine("App.ActiveDocument."+str(ln)+".addConstraint(Sketcher.Constraint('Coincident',"+str(layerCircleArcEntities[i][0])+",1,"+str(layerCircleArcEntities[j][0])+",2))")
          constraintCount=constraintCount+1
      if testCoincidence(arc1x2,arc1y2,arc2x1,arc2y1):
        if testFlatAngles(arc1a2,arc2a1) and constraintTangents:
          spitLine("App.ActiveDocument."+str(ln)+".addConstraint(Sketcher.Constraint('Tangent',"+str(layerCircleArcEntities[i][0])+",2,"+str(layerCircleArcEntities[j][0])+",1))")
          constraintCount=constraintCount+1
        else:
          spitLine("App.ActiveDocument."+str(ln)+".addConstraint(Sketcher.Constraint('Coincident',"+str(layerCircleArcEntities[i][0])+",2,"+str(layerCircleArcEntities[j][0])+",1))")
          constraintCount=constraintCount+1
      if testCoincidence(arc1x2,arc1y2,arc2x2,arc2y2):
        if testFlatAngles(arc1a2,arc2a2) and constraintTangents:
          spitLine("App.ActiveDocument."+str(ln)+".addConstraint(Sketcher.Constraint('Tangent',"+str(layerCircleArcEntities[i][0])+",2,"+str(layerCircleArcEntities[j][0])+",2))")
          constraintCount=constraintCount+1
        else:
          spitLine("App.ActiveDocument."+str(ln)+".addConstraint(Sketcher.Constraint('Coincident',"+str(layerCircleArcEntities[i][0])+",2,"+str(layerCircleArcEntities[j][0])+",2))")
          constraintCount=constraintCount+1

def stitchLinesToCircleArcs(ln):
  global layerLineEntities
  global layerCircleArcEntities
  global constraintCount
  for i in range(0,len(layerLineEntities)):
    line1x1=layerLineEntities[i][1]
    line1y1=layerLineEntities[i][2]
    line1x2=layerLineEntities[i][3]
    line1y2=layerLineEntities[i][4]
    line1a1=layerLineEntities[i][5]
    for j in range(0,len(layerCircleArcEntities)):
      arc1a1=layerCircleArcEntities[j][3]
      arc1a2=layerCircleArcEntities[j][4]
      arc1x1=layerCircleArcEntities[j][5]
      arc1y1=layerCircleArcEntities[j][6]
      arc1x2=layerCircleArcEntities[j][7]
      arc1y2=layerCircleArcEntities[j][8]
      if testCoincidence(line1x1,line1y1,arc1x1,arc1y1):
        if testPerpendicularAngles(line1a1,arc1a1) and constraintTangents:
          spitLine("App.ActiveDocument."+str(ln)+".addConstraint(Sketcher.Constraint('Tangent',"+str(layerLineEntities[i][0])+",1,"+str(layerCircleArcEntities[j][0])+",1))")
          constraintCount=constraintCount+1
        else:
          spitLine("App.ActiveDocument."+str(ln)+".addConstraint(Sketcher.Constraint('Coincident',"+str(layerLineEntities[i][0])+",1,"+str(layerCircleArcEntities[j][0])+",1))")
          constraintCount=constraintCount+1
      if testCoincidence(line1x1,line1y1,arc1x2,arc1y2):
        if testPerpendicularAngles(line1a1,arc1a2) and constraintTangents:
          spitLine("App.ActiveDocument."+str(ln)+".addConstraint(Sketcher.Constraint('Tangent',"+str(layerLineEntities[i][0])+",1,"+str(layerCircleArcEntities[j][0])+",2))")
          constraintCount=constraintCount+1
        else:
          spitLine("App.ActiveDocument."+str(ln)+".addConstraint(Sketcher.Constraint('Coincident',"+str(layerLineEntities[i][0])+",1,"+str(layerCircleArcEntities[j][0])+",2))")
          constraintCount=constraintCount+1
      if testCoincidence(line1x2,line1y2,arc1x1,arc1y1):
        if testPerpendicularAngles(line1a1,arc1a1) and constraintTangents:
          spitLine("App.ActiveDocument."+str(ln)+".addConstraint(Sketcher.Constraint('Tangent',"+str(layerLineEntities[i][0])+",2,"+str(layerCircleArcEntities[j][0])+",1))")
          constraintCount=constraintCount+1
        else:
          spitLine("App.ActiveDocument."+str(ln)+".addConstraint(Sketcher.Constraint('Coincident',"+str(layerLineEntities[i][0])+",2,"+str(layerCircleArcEntities[j][0])+",1))")
          constraintCount=constraintCount+1
      if testCoincidence(line1x2,line1y2,arc1x2,arc1y2):
        if testPerpendicularAngles(line1a1,arc1a2) and constraintTangents:
          spitLine("App.ActiveDocument."+str(ln)+".addConstraint(Sketcher.Constraint('Tangent',"+str(layerLineEntities[i][0])+",2,"+str(layerCircleArcEntities[j][0])+",2))")
          constraintCount=constraintCount+1
        else:
          spitLine("App.ActiveDocument."+str(ln)+".addConstraint(Sketcher.Constraint('Coincident',"+str(layerLineEntities[i][0])+",2,"+str(layerCircleArcEntities[j][0])+",2))")
          constraintCount=constraintCount+1

def stitchLinesToEllipseArcs(ln):
  global layerLineEntities
  global layerEllipseArcEntities
  global constraintCount
  for i in range(0,len(layerLineEntities)):
    line1x1=layerLineEntities[i][1]
    line1y1=layerLineEntities[i][2]
    line1x2=layerLineEntities[i][3]
    line1y2=layerLineEntities[i][4]
    line1a1=layerLineEntities[i][5]
    for j in range(0,len(layerEllipseArcEntities)):
      arc1x1=layerEllipseArcEntities[j][1]
      arc1y1=layerEllipseArcEntities[j][2]
      arc1x2=layerEllipseArcEntities[j][3]
      arc1y2=layerEllipseArcEntities[j][4]
      arc1a1=layerEllipseArcEntities[j][5]
      arc1a2=layerEllipseArcEntities[j][6]
      if testCoincidence(line1x1,line1y1,arc1x1,arc1y1):
        if testFlatAngles(line1a1,arc1a1) and constraintTangents:
          spitLine("App.ActiveDocument."+str(ln)+".addConstraint(Sketcher.Constraint('Tangent',"+str(layerLineEntities[i][0])+",1,"+str(layerEllipseArcEntities[j][0])+",1))")
          constraintCount=constraintCount+1
        else:
          spitLine("App.ActiveDocument."+str(ln)+".addConstraint(Sketcher.Constraint('Coincident',"+str(layerLineEntities[i][0])+",1,"+str(layerEllipseArcEntities[j][0])+",1))")
          constraintCount=constraintCount+1
      if testCoincidence(line1x1,line1y1,arc1x2,arc1y2):
        if testFlatAngles(line1a1,arc1a2) and constraintTangents:
          spitLine("App.ActiveDocument."+str(ln)+".addConstraint(Sketcher.Constraint('Tangent',"+str(layerLineEntities[i][0])+",1,"+str(layerEllipseArcEntities[j][0])+",2))")
          constraintCount=constraintCount+1
        else:
          spitLine("App.ActiveDocument."+str(ln)+".addConstraint(Sketcher.Constraint('Coincident',"+str(layerLineEntities[i][0])+",1,"+str(layerEllipseArcEntities[j][0])+",2))")
          constraintCount=constraintCount+1
      if testCoincidence(line1x2,line1y2,arc1x1,arc1y1):
        if testFlatAngles(line1a1,arc1a1) and constraintTangents:
          spitLine("App.ActiveDocument."+str(ln)+".addConstraint(Sketcher.Constraint('Tangent',"+str(layerLineEntities[i][0])+",2,"+str(layerEllipseArcEntities[j][0])+",1))")
          constraintCount=constraintCount+1
        else:
          spitLine("App.ActiveDocument."+str(ln)+".addConstraint(Sketcher.Constraint('Coincident',"+str(layerLineEntities[i][0])+",2,"+str(layerEllipseArcEntities[j][0])+",1))")
          constraintCount=constraintCount+1
      if testCoincidence(line1x2,line1y2,arc1x2,arc1y2):
        if testFlatAngles(line1a1,arc1a2) and constraintTangents:
          spitLine("App.ActiveDocument."+str(ln)+".addConstraint(Sketcher.Constraint('Tangent',"+str(layerLineEntities[i][0])+",2,"+str(layerEllipseArcEntities[j][0])+",2))")
          constraintCount=constraintCount+1
        else:
          spitLine("App.ActiveDocument."+str(ln)+".addConstraint(Sketcher.Constraint('Coincident',"+str(layerLineEntities[i][0])+",2,"+str(layerEllipseArcEntities[j][0])+",2))")
          constraintCount=constraintCount+1

def stitchCircleArcsToEllipseArcs(ln):
  global layerCircleArcEntities
  global layerEllipseArcEntities
  global constraintCount
  for i in range(0,len(layerCircleArcEntities)):
    arc1a1=layerCircleArcEntities[i][3]
    arc1a2=layerCircleArcEntities[i][4]
    arc1x1=layerCircleArcEntities[i][5]
    arc1y1=layerCircleArcEntities[i][6]
    arc1x2=layerCircleArcEntities[i][7]
    arc1y2=layerCircleArcEntities[i][8]
    for j in range(0,len(layerEllipseArcEntities)):
      eArc1x1=layerEllipseArcEntities[j][1]
      eArc1y1=layerEllipseArcEntities[j][2]
      eArc1x2=layerEllipseArcEntities[j][3]
      eArc1y2=layerEllipseArcEntities[j][4]
      eArc1a1=layerEllipseArcEntities[j][5]
      eArc1a2=layerEllipseArcEntities[j][6]
      if testCoincidence(arc1x1,arc1y1,eArc1x1,eArc1y1):
        if testPerpendicularAngles(arc1a1,eArc1a1) and constraintTangents:
          spitLine("App.ActiveDocument."+str(ln)+".addConstraint(Sketcher.Constraint('Tangent',"+str(layerCircleArcEntities[i][0])+",1,"+str(layerEllipseArcEntities[j][0])+",1))")
          constraintCount=constraintCount+1
        else:
          spitLine("App.ActiveDocument."+str(ln)+".addConstraint(Sketcher.Constraint('Coincident',"+str(layerCircleArcEntities[i][0])+",1,"+str(layerEllipseArcEntities[j][0])+",1))")
          constraintCount=constraintCount+1
      if testCoincidence(arc1x1,arc1y1,eArc1x2,eArc1y2):
        if testPerpendicularAngles(arc1a1,eArc1a2) and constraintTangents:
          spitLine("App.ActiveDocument."+str(ln)+".addConstraint(Sketcher.Constraint('Tangent',"+str(layerCircleArcEntities[i][0])+",1,"+str(layerEllipseArcEntities[j][0])+",2))")
          constraintCount=constraintCount+1
        else:
          spitLine("App.ActiveDocument."+str(ln)+".addConstraint(Sketcher.Constraint('Coincident',"+str(layerCircleArcEntities[i][0])+",1,"+str(layerEllipseArcEntities[j][0])+",2))")
          constraintCount=constraintCount+1
      if testCoincidence(arc1x2,arc1y2,eArc1x1,eArc1y1):
        if testPerpendicularAngles(arc1a2,eArc1a1) and constraintTangents:
          spitLine("App.ActiveDocument."+str(ln)+".addConstraint(Sketcher.Constraint('Tangent',"+str(layerCircleArcEntities[i][0])+",2,"+str(layerEllipseArcEntities[j][0])+",1))")
          constraintCount=constraintCount+1
        else:
          spitLine("App.ActiveDocument."+str(ln)+".addConstraint(Sketcher.Constraint('Coincident',"+str(layerCircleArcEntities[i][0])+",2,"+str(layerEllipseArcEntities[j][0])+",1))")
          constraintCount=constraintCount+1
      if testCoincidence(arc1x2,arc1y2,eArc1x2,eArc1y2):
        if testPerpendicularAngles(arc1a2,eArc1a2) and constraintTangents:
          spitLine("App.ActiveDocument."+str(ln)+".addConstraint(Sketcher.Constraint('Tangent',"+str(layerCircleArcEntities[i][0])+",2,"+str(layerEllipseArcEntities[j][0])+",2))")
          constraintCount=constraintCount+1
        else:
          spitLine("App.ActiveDocument."+str(ln)+".addConstraint(Sketcher.Constraint('Coincident',"+str(layerCircleArcEntities[i][0])+",2,"+str(layerEllipseArcEntities[j][0])+",2))")
          constraintCount=constraintCount+1

def stitchEllipseArcsToEllipseArcs(ln):
  global layerEllipseArcEntities
  global constraintCount
  for i in range(0,len(layerEllipseArcEntities)-1):
    eArc1x1=layerEllipseArcEntities[i][1]
    eArc1y1=layerEllipseArcEntities[i][2]
    eArc1x2=layerEllipseArcEntities[i][3]
    eArc1y2=layerEllipseArcEntities[i][4]
    eArc1a1=layerEllipseArcEntities[i][5]
    eArc1a2=layerEllipseArcEntities[i][6]
    for j in range(i+1,len(layerEllipseArcEntities)):
      eArc2x1=layerEllipseArcEntities[j][1]
      eArc2y1=layerEllipseArcEntities[j][2]
      eArc2x2=layerEllipseArcEntities[j][3]
      eArc2y2=layerEllipseArcEntities[j][4]
      eArc2a1=layerEllipseArcEntities[j][5]
      eArc2a2=layerEllipseArcEntities[j][6]
      if testCoincidence(eArc1x1,eArc1y1,eArc2x1,eArc2y1):
        if testFlatAngles(eArc1a1,eArc2a1) and constraintTangents:
          spitLine("App.ActiveDocument."+str(ln)+".addConstraint(Sketcher.Constraint('Tangent',"+str(layerEllipseArcEntities[i][0])+",1,"+str(layerEllipseArcEntities[j][0])+",1))")
          constraintCount=constraintCount+1
        else:
          spitLine("App.ActiveDocument."+str(ln)+".addConstraint(Sketcher.Constraint('Coincident',"+str(layerEllipseArcEntities[i][0])+",1,"+str(layerEllipseArcEntities[j][0])+",1))")
          constraintCount=constraintCount+1
      if testCoincidence(eArc1x1,eArc1y1,eArc2x2,eArc2y2):
        if testFlatAngles(eArc1a1,eArc2a2) and constraintTangents:
          spitLine("App.ActiveDocument."+str(ln)+".addConstraint(Sketcher.Constraint('Tangent',"+str(layerEllipseArcEntities[i][0])+",1,"+str(layerEllipseArcEntities[j][0])+",2))")
          constraintCount=constraintCount+1
        else:
          spitLine("App.ActiveDocument."+str(ln)+".addConstraint(Sketcher.Constraint('Coincident',"+str(layerEllipseArcEntities[i][0])+",1,"+str(layerEllipseArcEntities[j][0])+",2))")
          constraintCount=constraintCount+1
      if testCoincidence(eArc1x2,eArc1y2,eArc2x1,eArc2y1):
        if testFlatAngles(eArc1a2,eArc2a1) and constraintTangents:
          spitLine("App.ActiveDocument."+str(ln)+".addConstraint(Sketcher.Constraint('Tangent',"+str(layerEllipseArcEntities[i][0])+",2,"+str(layerEllipseArcEntities[j][0])+",1))")
          constraintCount=constraintCount+1
        else:
          spitLine("App.ActiveDocument."+str(ln)+".addConstraint(Sketcher.Constraint('Coincident',"+str(layerEllipseArcEntities[i][0])+",2,"+str(layerEllipseArcEntities[j][0])+",1))")
          constraintCount=constraintCount+1
      if testCoincidence(eArc1x2,eArc1y2,eArc2x2,eArc2y2):
        if testFlatAngles(eArc1a2,eArc2a2) and constraintTangents:
          spitLine("App.ActiveDocument."+str(ln)+".addConstraint(Sketcher.Constraint('Tangent',"+str(layerEllipseArcEntities[i][0])+",2,"+str(layerEllipseArcEntities[j][0])+",2))")
          constraintCount=constraintCount+1
        else:
          spitLine("App.ActiveDocument."+str(ln)+".addConstraint(Sketcher.Constraint('Coincident',"+str(layerEllipseArcEntities[i][0])+",2,"+str(layerEllipseArcEntities[j][0])+",2))")
          constraintCount=constraintCount+1

def createLine(s,x1,y1,x2,y2):
  global entityCount
  global layerLineEntities
  global constraintCount
  x1=round(x1,maxDecimalsNumber)
  y1=round(y1,maxDecimalsNumber)
  x2=round(x2,maxDecimalsNumber)
  y2=round(y2,maxDecimalsNumber)
  if x1==x2 and y1==y2:
    entityCount=entityCount-1
    return
  spitLine("App.ActiveDocument." + str(s) + ".addGeometry(Part.LineSegment(App.Vector(" + str(x1) + "," + str(y1) + ",0),App.Vector(" + str(x2) + "," + str(y2) + ",0)),False)")
  la=round(math.degrees(math.atan2(y2-y1,x2-x1)),maxDecimalsNumber)
  if la<0: la=la+360
  if la>=180: la=la-180
  ll=math.hypot(x2-x1,y2-y1)
  line=[entityCount,x1,y1,x2,y2,math.fabs(la)]
  layerLineEntities.append(line)
  if abs(y1-y2)<=lineTolerance and constraintHorizontalLines:
    spitLine("App.ActiveDocument." + str(s) + ".addConstraint(Sketcher.Constraint('Horizontal'," + str(entityCount) + "))")
    constraintCount=constraintCount+1
  if abs(x1-x2)<=lineTolerance and constraintVerticalLines:
    spitLine("App.ActiveDocument." + str(s) + ".addConstraint(Sketcher.Constraint('Vertical'," + str(entityCount) + "))")
    constraintCount=constraintCount+1
  if constraintLinesLengths:
    spitLine("App.ActiveDocument."+ str(s) + ".addConstraint(Sketcher.Constraint('Distance',"+ str(entityCount) +","+str(ll)+"))")
    constraintCount=constraintCount+1

def createCircle(s,x,y,r):
  global entityCount
  global constraintCount
  if r==0:
    entityCount=entityCount-1
    return
  x=round(x,maxDecimalsNumber)
  y=round(y,maxDecimalsNumber)
  r=round(r,maxDecimalsNumber)
  spitLine("App.ActiveDocument." + str(s) + ".addGeometry(Part.Circle(App.Vector(" + str(x) + "," + str(y) + ",0),App.Vector(0,0,1)," + str(r) + "),False)")
  if constraintRadius:
    spitLine("App.ActiveDocument." + str(s) + ".addConstraint(Sketcher.Constraint('Radius'," + str(entityCount) + "," + str(r) + "))")
    constraintCount=constraintCount+1
  if constraintOrigins:
    spitLine("App.ActiveDocument." + str(s) + ".addConstraint(Sketcher.Constraint('DistanceX'," + str(entityCount) + ",3," + str(x) + ")) ")
    constraintCount=constraintCount+1
    spitLine("App.ActiveDocument." + str(s) + ".addConstraint(Sketcher.Constraint('DistanceY'," + str(entityCount) + ",3," + str(y) + ")) ")
    constraintCount=constraintCount+1

def createCircleArc(s,x,y,r,a1,a2):
  global entityCount
  global constraintCount
  global digitsNumber
  x=round(x,maxDecimalsNumber)
  y=round(y,maxDecimalsNumber)
  r=round(r,maxDecimalsNumber)
  if a1==a2:
    entityCount=entityCount-1
    return
  if r==0:
    entityCount=entityCount-1
    return
  sa=round(math.degrees(a1),maxDecimalsNumber) # arc start angle
  ea=round(math.degrees(a2),maxDecimalsNumber) # arc end angle
  if sa<0: sa=sa+360 # eliminating negative angles
  if ea<0: ea=ea+360 # eliminating negative angles
  xs=round(x+r*math.cos(a1),maxDecimalsNumber) # xStart
  ys=round(y+r*math.sin(a1),maxDecimalsNumber) # yStart
  xe=round(x+r*math.cos(a2),maxDecimalsNumber) # xEnd
  ye=round(y+r*math.sin(a2),maxDecimalsNumber) # yEnd
  circleArc=[entityCount,x,y,sa,ea,xs,ys,xe,ye]
  layerCircleArcEntities.append(circleArc)
  spitLine("App.ActiveDocument." + str(s) + ".addGeometry(Part.ArcOfCircle(Part.Circle(App.Vector(" + str(x) + "," + str(y) + ",0),App.Vector(0,0,1)," + str(r) + ")," + str(a1) + "," + str(a2) + "),False)")
  if constraintRadius:
    spitLine("App.ActiveDocument." + str(s) + ".addConstraint(Sketcher.Constraint('Radius'," + str(entityCount) + "," + str(r) + "))")
    constraintCount=constraintCount+1
  if constraintOrigins:
    spitLine("App.ActiveDocument." + str(s) + ".addConstraint(Sketcher.Constraint('DistanceX'," + str(entityCount) + ",3," + str(x) + ")) ")
    constraintCount=constraintCount+1
    spitLine("App.ActiveDocument." + str(s) + ".addConstraint(Sketcher.Constraint('DistanceY'," + str(entityCount) + ",3," + str(y) + ")) ")
    constraintCount=constraintCount+1

def createEllipse(s,xo,yo,xa,ya,r):
  global entityCount
  xo=round(xo,maxDecimalsNumber)
  yo=round(yo,maxDecimalsNumber)
  xa=round(xa,maxDecimalsNumber)
  ya=round(ya,maxDecimalsNumber)
  a=math.hypot(xa,ya) # longuest length of the ellipse
  b=a*r # shortest length of the ellipse
  c=math.sqrt(a*a-b*b) # focus length of the ellipse
  ae=math.atan2(ya,xa) # rotation angle of the ellipse
  spitLine("App.ActiveDocument." + str(s) + ".addGeometry(Part.Ellipse(App.Vector(" + str(xo+xa) + "," + str(yo+ya) + ",0),App.Vector(" + str(xo-ya*r) + "," + str(yo+xa*r) + ",0),App.Vector(" + str(xo) + "," + str(yo) + ",0)),False)")
  if exposeGeometry:
    spitLine("App.ActiveDocument." + str(s) + ".exposeInternalGeometry("+str(entityCount)+")")
    entityCount=entityCount+4

def createEllipseArc(s,xo,yo,xa,ya,r,a1,a2):
  global entityCount
  global layerEllipseArcEntities
  xo=round(xo,maxDecimalsNumber)
  yo=round(yo,maxDecimalsNumber)
  xa=round(xa,maxDecimalsNumber)
  ya=round(ya,maxDecimalsNumber)
  a=math.hypot(xa,ya) # longuest length of the ellipse
  b=a*r # shortest length of the ellipse
  c=math.sqrt(a*a-b*b) # focus length of the ellipse
  ae=math.atan2(ya,xa) # rotation angle of the ellipse
  # distance from ending points of the ellipse arc to the center
  lp1=math.hypot(a*math.cos(a1),b*math.sin(a1))
  lp2=math.hypot(a*math.cos(a2),b*math.sin(a2))
  # effective angles from center to points
  ac1=math.atan2(a*math.sin(a1)*r,math.cos(a1)*a)
  ac2=math.atan2(a*math.sin(a2)*r,math.cos(a2)*a)
  # ellipse arc ending points position
  xs=xo+lp1*(math.cos(ac1)*math.cos(ae)-math.sin(ac1)*math.sin(ae))# (cosA*cosB-sinA*sinB)
  ys=yo+lp1*(math.sin(ac1)*math.cos(ae)+math.sin(ae)*math.cos(ac1))# (sinA*cosB+sinB*cosA)
  xe=xo+lp2*(math.cos(ac2)*math.cos(ae)-math.sin(ac2)*math.sin(ae))
  ye=yo+lp2*(math.sin(ac2)*math.cos(ae)+math.sin(ae)*math.cos(ac2))
  # ellipse arc start and end slope angles
  if a1==0 or a1==math.pi or a1==math.pi*2:
    ssa=90
  else:
    ssa=math.degrees(math.atan(-math.cos(a1)/math.sin(a1)*r))+math.degrees(ae)
  if a2==0 or a2==math.pi or a2==math.pi*2:
    esa=90
  else:
    esa=math.degrees(math.atan(-math.cos(a2)/math.sin(a2)*r))+math.degrees(ae)
  ellipseArc=[entityCount,xs,ys,xe,ye,ssa,esa] # [EntityNumber,xStart,yStart,xEnd,yEnd,startSlopeAngle,endSlopeAngle]
  layerEllipseArcEntities.append(ellipseArc)
  spitLine("App.ActiveDocument." + str(s) + ".addGeometry(Part.ArcOfEllipse(Part.Ellipse(App.Vector(" + str(xo+xa) + "," + str(yo+ya) + ",0),App.Vector(" + str(xo-ya*r) + "," + str(yo+xa*r) + ",0),App.Vector(" + str(xo) + "," + str(yo) + ",0)),"+ str(a1) +","+ str(a2) +"),False)")
  if exposeGeometry:
    spitLine("App.ActiveDocument." + str(s) + ".exposeInternalGeometry("+str(entityCount)+")")
    entityCount=entityCount+4

# Maybe create a 'main' function to put all the following mess..
processOptions(sys.argv[1:]) # Get and verify a bit options from the command line
DXF_Data=ezdxf.readfile(inputFile) # Read the DXF and store the data
msp = DXF_Data.modelspace()
# List the layers names
for layer in DXF_Data.layers:
  if debugMode:
    print("\n# Layer '"+str(layer.dxf.name)+"'")
    print("## Layer Flags:'"+str(layer.dxf.flags)+"'")
    print("## Layer Color:'"+str(layer.get_color())+"'")
    if layer.is_on(): print("## Layer is On")
    if layer.is_off(): print("## Layer is Off")
    if layer.is_frozen(): print("## Layer is Frozen")
    if layer.is_locked(): print("## Layer is Locked")
  layerNames.append(str(layer.dxf.name))
# If a specified layer name has been given, check if exists
if selectedLayer:
  if selectedLayer in layerNames:
    if debugMode: print("## Selected Layer '"+selectedLayer+"' is present")
  else:
    print("Error: Layer not present in DXF file.\nAvailable Layers:")
    for l in layerNames: print(l)
    sys.exit(5)
# Adjust Tolerances
angleTolerance=float(float(angleMinutes)/60)+fuckingFloatToBinaryError # Angle tolerance for tangentiality test
lineTolerance=math.pow(10,-digitsNumber)+fuckingFloatToBinaryError # line tolerance for coincidence test
# Here starts the FreeCAD's document generation
createDocument(documentName.replace("-","_")) # replace dashes in document name and create the FreeCAD document
# Layers treatment
for layer in DXF_Data.layers:
  if str(layer.dxf.name)[:1].isdigit():
    nln=numericLayerPrefix+str(layer.dxf.name) # remame layers starting with a digit..
  else: nln=layer.dxf.name
  nln=nln.replace(" ","_") # replace spaces in layer names..
  nln=nln.replace("-","_") # replace dashes in layer names..
  if nln!=layer.dxf.name:
    if debugMode: print("## Renamed Layer '"+str(layer.dxf.name)+"' to '"+str(nln)+"'")
  if selectedLayer=='':
    if visibleLayers and layer.is_frozen(): # LibreCad uses the is_frozen flag for hidden layers..
      if debugMode: print("## Skipping Invisible Layer '"+str(layer.dxf.name)+"'")
    else:
      processLayer(layer.dxf.name,nln)
      if constraintCoincidences:
        if layerLineEntities: stitchLinesToLines(nln)
        if layerLineEntities and layerCircleArcEntities: stitchLinesToCircleArcs(nln)
        if layerCircleArcEntities: stitchCircleArcsToCircleArcs(nln)
        if layerLineEntities and layerEllipseArcEntities: stitchLinesToEllipseArcs(nln)
        if layerCircleArcEntities and layerEllipseArcEntities: stitchCircleArcsToEllipseArcs(nln)
        if layerEllipseArcEntities: stitchEllipseArcsToEllipseArcs(nln)
      if debugMode: print("## Constraints created for layer '"+str(nln)+"' : "+str(constraintCount))
      layerLineEntities=[]
      layerCircleArcEntities=[]
      layerEllipseArcEntities=[]
  else:
    if selectedLayer==str(layer.dxf.name):
      processLayer(layer.dxf.name,nln)
      if layerLineEntities: stitchLinesToLines(nln)
      if layerLineEntities and layerCircleArcEntities: stitchLinesToCircleArcs(nln)
      if layerCircleArcEntities: stitchCircleArcsToCircleArcs(nln)
      if layerLineEntities and layerEllipseArcEntities: stitchLinesToEllipseArcs(nln)
      if layerCircleArcEntities and layerEllipseArcEntities: stitchCircleArcsToEllipseArcs(nln)
      if layerEllipseArcEntities: stitchEllipseArcsToEllipseArcs(nln)
      if debugMode: print("## Constraints created for layer '"+str(nln)+"' : "+str(constraintCount))
      layerLineEntities=[]
      layerCircleArcEntities=[]
      layerEllipseArcEntities=[]
# Document end stuff
spitLine("App.ActiveDocument.recompute()")
saveFcstdFile(documentName) # Tell FreeCAD to save the document
# Close the opened Output file if any
if outputFile:
  fileHandle.close()
